#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	srand(time(0));
	//definicion de variable
	int n, numero_aleatorio;
	//el usuario ingresa los datos de la matriz para almacenarlos
	printf("por favor escriba el tamano de la matriz:\n");
	scanf("%d", &n);
	printf("por favor escriba el numero por el cual se van a generar la matriz de manera aleatoria:\n");
	scanf("%d", &numero_aleatorio);
	//esta variable almazena el tamano de la matriz
	int matriz[n][n];
	//numeros al azar menores que el se por ingreso por el usuario
	for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				//esta funcion genera los numeros aleatorios dentro de la matriz
				matriz[i][j] = rand()%numero_aleatorio;
		}
	}
		//se imprime la matriz original
		printf("\nmatriz original\n");
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				printf("|%d|", matriz[i][j]);
			}
			printf("\n");
		}
		//se imprime la matriz intercambiada
		printf("\nmatriz intercambiada\n");	
		//para esto simplementre en el contador se agregan los datos dados en el problema
		for(int i=n-1;i>-1;i--){
		for(int j=0;j<n;j++){
			printf("|%d|", matriz[i][j]);
			}
			printf("\n");
		}
	return 0;
}

